class EligibilityService {
  /**
   * Compare cart data with criteria to compute eligibility.
   * If all criteria are fulfilled then the cart is eligible (return true).
   *
   * @param cart
   * @param criteria
   * @return {boolean}
   */
  isEligible(cart, criteria) {
    const checkCondition = (value, condition) => {
      if (typeof condition === "object") {
        const [operator, targetValue] = Object.entries(condition)[0];
        switch (operator) {
          case "gt":
            return value > targetValue;
          case "lt":
            return value < targetValue;
          case "gte":
            return value >= targetValue;
          case "lte":
            return value <= targetValue;
          case "in":
            return [value].some((v) => targetValue.includes(v));
          case "and":
            return Object.entries(condition["and"]).every(
              ([subOperator, subCriteria]) => {
                const subCondition = {
                  [subOperator]: subCriteria,
                };
                return checkCondition(value, subCondition);
              }
            );
          case "or":
            return Object.entries(condition["or"]).some(
              ([subOperator, subCriteria]) => {
                const subCondition = {
                  [subOperator]: subCriteria,
                };
                return checkCondition(value, subCondition);
              }
            );
          default:
            return false;
        }
      } else {
        return value == condition;
      }
    };

    for (const [criteria_key, condition] of Object.entries(criteria)) {
      const is_complex_criteria = criteria_key.includes(".");
      if (is_complex_criteria) {
        const criteria_keys = criteria_key.split(".");
        const first_criteria_key = criteria_keys[0];
        const second_criteria_key = criteria_keys[1];

        const cart_value = cart[first_criteria_key];
        const does_cart_condition_exist = cart_value != undefined;

        if (does_cart_condition_exist) {
          const is_cart_value_array = Array.isArray(cart_value);
          const is_cart_value_object = typeof cart_value === "object";
          if (is_cart_value_array) {
            const result = cart_value.some((value) => {
              const value_condition_to_check = value[second_criteria_key];
              return checkCondition(value_condition_to_check, condition);
            });
            return result;
          } else if (is_cart_value_object) {
            const value_condition_to_check = cart_value[second_criteria_key];
            return checkCondition(value_condition_to_check, condition);
          }
        } else {
          return false;
        }
      } else {
        const cart_value = cart[criteria_key];
        const does_cart_condition_exist = cart_value != undefined;
        if (does_cart_condition_exist) {
          return checkCondition(cart_value, condition);
        } else {
          return false;
        }
      }
    }

    return true;
  }
}

module.exports = {
  EligibilityService,
};
